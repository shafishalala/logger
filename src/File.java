class FileLogger implements Logger {
    @Override
    public void logInfo(String message) {
        writeToFile("[INFO] " + message);
    }

    @Override
    public void logWarning(String message) {
        writeToFile("[WARNING] " + message);
    }

    @Override
    public void logError(String message) {
        writeToFile("[ERROR] " + message);
    }

    private void writeToFile(String message) {
        // Code to write the message to a log file goes here
        // You can use FileWriter or any other file handling mechanism
        // For simplicity, let's just print the message to the console
        System.out.println("Writing to log file: " + message);
    }
}
